# Gitlab Build to AWS

This document is to layout the gitlab-cc install and automation for AWS as an infrastructure. 

## Prerequisites
An AWS account
A basic understanding of Linux concepts 
A basic understanding of git and gitlab concepts

## Infrustructure Components.  

Here are the components to build a complete HA enterprise gitlab infrustructure on AWS.

### NFS on DRBD 
Gitlab currently does not support EFS for a Highly Available (HA) data storage.  The primary reason is the performance of EFS reading and writing small files such as those stored in git.

DRBD is a software-based, shared-nothing, replicated storage solution mirroring the content of block devices (hard disks, partitions, logical volumes etc.) between hosts. DRBD mirrors data in real time. Replication occurs continuously while applications modify the data on the device. DRBD mirrors data transparently, applications need not be aware that the data is stored on multiple hosts. DRBD mirrors data synchronously or asynchronously. With synchronous mirroring, applications are notified of write completions after the writes have been carried out on all (connected) hosts. With asynchronous mirroring, applications are notified of write completions when the writes have completed locally, which usually is before they have propagated to the other hosts.

The Network File System (NFS) is a way of mounting Linux discs/directories over a network. An NFS server can export one or more directories that can then be mounted on a remote Linux machine.

Together these 2 technologies bring together a high performance HA data storage that can be replicated across Availability zones or even regions in AWS. In this document I will layout the manual installation and automation of the DRBD/NFS solution used in Gitlab Production in AWS.  

#### What things you need to install the software and how to install them

#### Manual Set up of the DRBD/NFS cluster in AWS
*Launch 2 EC2 Instance with these configuration outliers. (provided in CF template)  
*Latest Redhat Image
*r4.large instance type Cheapest option for 10G Network, ebs optimized, 2vcpu , 15.25g enhanced memory
*Volumes, 30GiB encrypted root, Two 1100GiB encrypted provisioned IOPs at 3500 IOPs 
*Security Group:  open port="7789" for DRBD.  Port 111 (TCP and UDP) and 2049 (TCP and UDP) for the NFS server. There are also ports for Cluster and client status (Port 1110 TCP for the former, and 1110 UDP for the latter) as well as a port for the NFS lock manager (Port 4045 TCP and UDP).
*Login to your new Instances
*sudo yum update
*sudo yum install nfs-utils nfs-utils-lib lvm2 -y
*rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
*rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
*sudo yum install drbd90-utils kmod-drbd90 -y
*sudo modprobe drbd
*sudo lsmod | grep -I drbd
*this temporarily adds the module to make persistent on reboot run: echo drbd > /etc/modules-load.d/drbd.conf
*you should see drbd output
*possible reboot
*Create logical volumes
*pvcreate /dev/xvdf
*pvcreate dev/xvdg
*vgcreate vgdrbdnfs /dev/xvdf
*vgcreate vgdrbdreg /dev/xvdg
*lvcreate -n nfs -l100%FREE vgdrbdnfs
*lvcreate -n registry -l100%FREE vgdrbdreg
*Path to logical volumes:
*/dev/vgdrbdnfs/nfs
*/dev/vgdrbdreg/registry
*Configure DRBD
*Configuration files in drbd/config
*Copy the configuration files to /etc/drbd.d/ 
*rename service-nfs.res.template to service-nfs.res
*drbdadm create-md service-nfs
*systemctl start service-nfs
*systemctl enable service-nfs
*drbdadm up service-nfs
*drbdadm primary service-nfs
*Note:  if you get any error to make the node primary, use the following command to forcefully make the node as primary:
drbdadm primary drbd --force
*You can check the current status of the synchronization while it’s being performed. The cat /proc/drbd command displays the creation and synchronization progress of the resource, as shown here:
*cat /proc/service-nfs
*mkfs.xfs /dev/drbd1
*mkfs.xfs /dev/drbd2
*mkdir /var/opt/gitlab-nfs
*mkdir /var/opt/gitlab-registry
*mount /dev/drbd1 /var/opt/gitlab-nfs
*mount /dev/drbd2 /var/opt/gitlab-registry




```
Give examples
```


## Contributing


## Versioning


## Authors


## Acknowledgments
